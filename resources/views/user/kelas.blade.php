@extends('layouts.userpage')

@section('title',"Cocoding")

@section('content')
  
{{-- Banner --}}
<div class="wrap-banner" style="background-image: url('{{url('assets/img/img3.jpg')}}')">
    <div class="banner">
      <h1>YUK BELAJAR NGODING</h1>
      <p>Cocoding punya banyak kelas-kelas yang bisa kamu ikuti</p>
      <div>
        <a href="" class="btn btn-lm mr-3">Mulai Dari Mana ?</a>
        <a href="" class="btn btn-gs">Gabung Sekarang</a>
      </div>
    </div>
</div>
{{-- End Banner --}}

{{-- Class --}}
<div class="wrap-class class-page">
  <div class="container">
      <div class="class">
        <h5>KELAS PEMULA</h5>
        <span class="h-separator"></span>

        <div class="row">

          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
        </div>
  </div>
</div>
{{-- End Class --}}
{{-- Class --}}
<div class="wrap-class class-page">
  <div class="container">
      <div class="class">
        <h5>KELAS PEMULA</h5>
        <span class="h-separator"></span>

        <div class="row">

          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>

          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>

          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>

          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
          
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
          <div class="col-3">
            <div class="item">
              <img src="{{url("assets/img/img4.jpg")}}" alt="">
              <div class="text">
                <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
                <div class="d-flex justify-content-between">
                  <p>Pemula</p>
                  <p><i class="fas fa-users"></i> 8890</p>
                </div>
              </div>
              <a href="" class="btn border">Lihat Kelas</a>
            </div>
          </div>
  
        </div>
  </div>
</div>
{{-- End Class --}}
@endsection