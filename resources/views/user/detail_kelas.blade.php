@extends('layouts.userpage')

@section('title',"Cocoding")

@section('content')

<div class="wrap-class-thumb">
  <div class="container">
    <div class="class-thumb">
      <div class="row">

        <div class="col-5 pr-5">
          <img src="{{url("assets/img/img4.jpg")}}" style="border-radius:2px" alt="">
        </div>

        <div class="col-7 d-flex align-items-center">
          <div>
            <h5>BELAJAR HTML DASAR UNTUK PEMULA</h5>

            <div class="d-flex justify-content-between w-50 mt-3">
              <p>Pemula</p>
              <p><i class="fas fa-play"></i> 8890</p>
              <p><i class="fas fa-users"></i> 8890</p>
            </div>

            <div>
              <a href="" class="btn btn-gs mr-3">Gabung Sekarang</a>
              <a href="" class="btn btn-lm">Pelajari Lebih</a>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection