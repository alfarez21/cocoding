@extends('layouts.userpage')

@section('title',"Cocoding")

@section('content')
  
{{-- Banner --}}
<div class="wrap-banner" style="background-image: url('{{url('assets/img/img4.jpg')}}')">
    <div class="banner">
      <h1>COCODING ECOURSE</h1>
      <p>Yuk belajar koding di cocoding, waktu belajar fleksibel, dimanapun, kapanpun, dan tentunya terstruktur.</p>
      <div>
        <a href="" class="btn btn-lm mr-3">Pelajari Lebih</a>
        <a href="" class="btn btn-gs">Gabung Sekarang</a>
      </div>
    </div>
</div>
{{-- End Banner --}}

{{-- About --}}
<div class="wrap-about">
  <div class="container">
      <div class="about">
        <div class="row">

          <div class="col-6 text">
            <h5>TENTANG COCODING</h5>
            <span class="h-separator"></span>
            <p>Cocoding adalah website belajar pemogrgraman online yang diperutukan bagi orang-orang yang tidak ada waktu untuk belajar. Belajar di cocoding sangatlah fleksibel, kamu bisa belajar dimanapun kapanpu dan tentunya terarah.</p>
            <p>Ada banyak kelas yang dapat kamu ikuti dan tentu di setiap kelas kita akan dibimbing oleh para tutor yang berpengalaman di bidang IT. cocoding akan membimbingmu sampai kamu bisa membuat program aplikasi</p>
          </div>

          <div class="col-6 img">
            <img src="{{url("assets/img/ilustration/ilu1.png")}}" class="w-100" alt="">
          </div>
        </div>
        </div>
  </div>
</div>
{{-- End About --}}

{{-- Testimoni --}}
<div class="wrap-testi">
  <div class="container">
    <div class="testi">

      <h5>KATA MEREKA ??</h5>
      <span class="h-separator"></span>
      <p>Berikut beberapa testimoni dari orang-orang yang sudah mengikuti cocoding</p>

      <div class="owl-carousel owl-theme owl-loaded testimo">
          <div class="owl-stage-outer">
              <div class="owl-stage mb-3">
                  <div class="owl-item item">
                    <div class="wrap-img border m-auto">
                      <img src="{{url("assets/img/face.jpg")}}" alt="">
                    </div>
                    <q>Recomended buat temen temen yang mau belajar koding, pokonya Cocoding mantep :v</q>
                    <p>- Muhamad Rifky -</p>
                </div>
                <div class="owl-item item">
                    <div class="wrap-img border m-auto">
                      <img src="{{url("assets/img/face.jpg")}}" alt="">
                    </div>
                    <q>Recomended buat temen temen yang mau belajar koding, pokonya Cocoding mantep :v</q>
                    <p>- Muhamad Rifky -</p>
                </div>
                <div class="owl-item item">
                    <div class="wrap-img border m-auto">
                      <img src="{{url("assets/img/face.jpg")}}" alt="">
                    </div>
                    <q>Recomended buat temen temen yang mau belajar koding, pokonya Cocoding mantep :v</q>
                    <p>- Muhamad Rifky -</p>
                </div>
                <div class="owl-item item">
                    <div class="wrap-img border m-auto">
                      <img src="{{url("assets/img/face.jpg")}}" alt="">
                    </div>
                    <q>Recomended buat temen temen yang mau belajar koding, pokonya Cocoding mantep :v</q>
                    <p>- Muhamad Rifky -</p>
                </div>
                <div class="owl-item item">
                    <div class="wrap-img border m-auto">
                      <img src="{{url("assets/img/face.jpg")}}" alt="">
                    </div>
                    <q>Recomended buat temen temen yang mau belajar koding, pokonya Cocoding mantep :v</q>
                    <p>- Muhamad Rifky -</p>
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
{{-- End Testimoni --}}


{{-- Techologi --}}
<div class="wrap-techno">
  <div class="container">
    <div class="techno">

      <h5>TEKNOLOGI YANG DIPELAJAR </h5>
      <span class="h-separator"></span>
      <p>Berikut beberapa teknologi yang akan dipelajari di cocoding</p>
        <div class="row">
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
          <div class="col-2">
            <div class="item">
              <img src="{{url("assets/img/html-5.png")}}" alt="">
              <h6>HTML</h6>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
{{-- End Techologi --}}


{{-- Class --}}
<div class="wrap-class">
  <div class="container">
    <div class="class">

      <h5>KELAS FAVORIT </h5>
      <span class="h-separator"></span>
      <p>Yuk ikuti kelas favorit di cocoding</p>

      <div class="row">

        <div class="col-3">
          <div class="item">
            <img src="{{url("assets/img/img4.jpg")}}" alt="">
            <div class="text">
              <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
              <div class="d-flex justify-content-between">
                <p>Pemula</p>
                <p><i class="fas fa-users"></i> 8890</p>
              </div>
            </div>
            <a href="" class="btn border">Lihat Kelas</a>
          </div>
        </div>

        <div class="col-3">
          <div class="item">
            <img src="{{url("assets/img/img4.jpg")}}" alt="">
            <div class="text">
              <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
              <div class="d-flex justify-content-between">
                <p>Pemula</p>
                <p><i class="fas fa-users"></i> 8890</p>
              </div>
            </div>
            <a href="" class="btn border">Lihat Kelas</a>
          </div>
        </div>
        
        <div class="col-3">
          <div class="item">
            <img src="{{url("assets/img/img4.jpg")}}" alt="">
            <div class="text">
              <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
              <div class="d-flex justify-content-between">
                <p>Pemula</p>
                <p><i class="fas fa-users"></i> 8890</p>
              </div>
            </div>
            <a href="" class="btn border">Lihat Kelas</a>
          </div>
        </div>

        <div class="col-3">
          <div class="item">
            <img src="{{url("assets/img/img4.jpg")}}" alt="">
            <div class="text">
              <h6>BELAJAR HTML DASAR UNTUK PEMULA</h6>
              <div class="d-flex justify-content-between">
                <p>Pemula</p>
                <p><i class="fas fa-users"></i> 8890</p>
              </div>
            </div>
            <a href="" class="btn border">Lihat Kelas</a>
          </div>
        </div>

      </div>

      <a href="" class="btn btn-lsk">Lihat Semua Kelas</a>
    </div>
  </div>
</div>
{{-- End Class --}}
@endsection