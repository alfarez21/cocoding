<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>

  {{-- STYLESHEET
  =====================================--}}

  {{-- Bootstrap 4 --}}
  <link rel="stylesheet" href="{{ url("vendors/bootstrap4/css/bootstrap.min.css")}}">
  {{-- Owl-carousel --}}
  <link rel="stylesheet" href="{{url("vendors/owlcarousel2/css/owl.carousel.min.css")}}">
  <link rel="stylesheet" href="{{url("vendors/owlcarousel2/css/owl.theme.default.min.css")}}">
  {{-- Fontawesome --}}
  <link rel="stylesheet" href="{{url("vendors/fontawesome5/css/all.css")}}">
  {{-- My Style --}}
  <link rel="stylesheet" href="{{ url("assets/css/style.css")}}">
  
  {{-- Font --}}
  <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap" rel="stylesheet"> 

</head>
<body>

  {{-- Navbar --}}
  <nav class="navbar py-3 navbar-expand-sm bg-white justify-content-between shadow">
    <div class="container">
      
      {{-- Brand --}}
      <a class="navbar-brand" href="{{url("/")}}"><strong>COCODING</strong></a>

      {{-- Nav Menu Center --}}
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{url("kelas")}}">KELAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">ARTIKEL</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">KONTAK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">TENTANG</a>
        </li>
      </ul>
      {{-- End Nav Menu Center --}}

      {{-- Nav Menu right--}}
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">MASUK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">DAFTAR</a>
        </li>
      </ul>
      {{-- End Nav Menu right--}}

    </div>
  </nav>
  {{--End Navbar --}}

  {{-- Content --}}
  @yield('content')
  {{-- End Content --}}

  {{-- Javascript --}}
  {{-- Jquery 3 --}}
  <script src="{{url("vendors/jquery3/jquery-3.4.1.min.js")}}"></script>
  {{-- Popper --}}
  <script src="{{url("vendors/popper/popper.min.js")}}"></script>
  {{-- Bootstrap 4 --}}
  <script src="{{url("vendors/bootstrap4/js/bootstrap.min.js")}}"></script>
  {{-- Owl Carousel 2 --}}
  <script src="{{url("vendors/owlcarousel2/js/owl.carousel.min.js")}}"></script>
  {{-- My Script --}}
  <script src="{{url("assets/js/script.js")}}"></script>
  
</body>
</html>